

void main()
{
    for (uint m=0; m<testNumber; ++m)
    {
        array<float> result = 
        {
            1.0, 0, 0, 0,
            0, 1.0, 0, 0, 
            0, 0, 1.0, 0, 
            0, 0, 0, 1.0
        };
        for (uint i=0; i<matrices.length(); ++i)
        {
            array<float>@ matrix = matrices[i];
            array<float> preresult(16, 0);
            for (uint j=0; j<4; ++j)
            {
                for (uint k=0; k<4; ++k)
                {
                    for (uint n=0; n<4; ++n)
                    {
                        preresult[j + k*4] += result[j + n*4] * matrix[k*4 + n];
                    }
                }
            }
            result = preresult;
        }
    }
}
