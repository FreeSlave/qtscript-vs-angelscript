

function main()
{
    for (var m=0; m<testNumber; ++m)
    {
        var result = 
        [
            1.0, 0, 0, 0,
            0, 1.0, 0, 0, 
            0, 0, 1.0, 0, 
            0, 0, 0, 1.0
        ];
        for (var i=0; i<matrices.length; ++i)
        {
            var matrix = matrices[i];
            var preresult = new Array(16);
            for (var j=0; j<4; ++j)
            {
                for (var k=0; k<4; ++k)
                {
                    preresult[j + k*4] = 0;
                    for (var n=0; n<4; ++n)
                    {
                        preresult[j + k*4] += result[j + n*4] * matrix[k*4 + n];
                    }
                }
            }
            result = preresult;
        }
    }
}
