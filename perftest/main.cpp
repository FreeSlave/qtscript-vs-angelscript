#include <QCoreApplication>
#include <QScriptEngine>
#include <QScriptValue>
#include <QTextStream>
#include <QVector>
#include <QMatrix4x4>
#include <random>
#include <cstdio>
#include <iostream>
#include <QFile>
#include <cassert>
#include <QTime>

#include <angelscript.h>
#include "scriptbuilder/scriptbuilder.h"
#include "scriptarray/scriptarray.h"

QScriptValue qtPrintResults(QScriptContext *context, QScriptEngine *)
{
    QScriptValue result = context->argument(0);
    for (quint32 i=0; i<16; ++i)
    {
        std::cout << result.property(i).toNumber() << ' ';
    }
    std::cout << std::endl;
    return QScriptValue();
}

void asPrintResults(CScriptArray* result)
{
    for (asUINT i=0; i<result->GetSize(); ++i)
    {
        float f = *(float*)result->At(i);
        std::cout << f << ' ';
    }
    std::cout << std::endl;
}

void MessageCallback(const asSMessageInfo *msg, void *)
{
    const char *type = "ERR ";
    if( msg->type == asMSGTYPE_WARNING )
        type = "WARN";
    else if( msg->type == asMSGTYPE_INFORMATION )
        type = "INFO";
    printf("%s (%d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message);
}

void testQtScript(const QVector<QMatrix4x4>& matrices, uint testNumber)
{
    QScriptEngine engine;
    QScriptValue qtScriptFunction = engine.newFunction(qtPrintResults);
    QScriptValue qtScriptMatrices = engine.newArray(matrices.count());
    for (int i=0; i<matrices.count(); ++i)
    {
        QScriptValue qtScriptMatrix = engine.newArray(16);
        const float* data = matrices[i].constData();
        for (int j=0; j<16; ++j)
        {
            qtScriptMatrix.setProperty(j, QScriptValue(&engine, data[j]));
        }
        qtScriptMatrices.setProperty(i, qtScriptMatrix);
    }

    engine.globalObject().setProperty("matrices", qtScriptMatrices);
    engine.globalObject().setProperty("printResults", qtScriptFunction);
    engine.globalObject().setProperty("testNumber", QScriptValue(testNumber));

    QFile qtScriptFile("../perftest/test.js");
    if (qtScriptFile.open(QIODevice::ReadOnly))
    {
        engine.evaluate(qtScriptFile.readAll());
        QScriptValue mainFunction = engine.globalObject().property("main");
        QTime timer;
        timer.start();
        mainFunction.call();
        int elapsed = timer.elapsed();
        std::cout << "QtScript: " << elapsed << std::endl;
        if (engine.hasUncaughtException())
        {
            std::cerr << "Exception was thrown from script" << std::endl;
        }
    }
    else
    {
        std::cerr << "Could not open file" << std::endl;
    }
}

void testAngelScript(const QVector<QMatrix4x4>& matrices, uint testNumber)
{
    asIScriptEngine *engine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
    int r = engine->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL); assert( r >= 0 );
    RegisterScriptArray(engine, true);
    asIObjectType* matricesType = engine->GetObjectTypeByDecl("array<array<float> >");
    asIObjectType* matrixType = engine->GetObjectTypeByDecl("array<float>");
    CScriptArray* asMatrices = CScriptArray::Create(matricesType, matrices.count());

    for (int i=0; i<matrices.size(); ++i)
    {
        CScriptArray* asMatrix = CScriptArray::Create(matrixType, 16);
        const float* data = matrices[i].constData();
        for (asUINT j=0; j<16; ++j)
        {
            float f = data[j];
            asMatrix->SetValue(j, &f);
        }
        asMatrices->SetValue(i, asMatrix);
        asMatrix->Release();
    }

    r = engine->RegisterGlobalFunction("void printResults(array<float>& in)", asFUNCTION(asPrintResults), asCALL_CDECL); assert( r >= 0 );
    r = engine->RegisterGlobalProperty("array<array<float> > matrices", asMatrices); assert(r >= 0);
    r = engine->RegisterGlobalProperty("uint testNumber", &testNumber); assert(r >= 0);

    CScriptBuilder builder;
    r = builder.StartNewModule(engine, "MyModule");
    if( r < 0 )
    {
        printf("Unrecoverable error while starting a new module.\n");
        return;
    }
    r = builder.AddSectionFromFile("../perftest/test.as");
    if( r < 0 )
    {
        printf("Please correct the errors in the script and try again.\n");
        return;
    }
    r = builder.BuildModule();
    if( r < 0 )
    {
        printf("Please correct the errors in the script and try again.\n");
        return;
    }

    asIScriptModule *mod = engine->GetModule("MyModule");
    asIScriptFunction *func = mod->GetFunctionByDecl("void main()");
    if( func == 0 )
    {
        printf("The script must have the function 'void main()'. Please add it and try again.\n");
        return;
    }

    asIScriptContext *ctx = engine->CreateContext();
    ctx->Prepare(func);
    QTime timer;
    timer.start();
    r = ctx->Execute();
    int elapsed = timer.elapsed();
    std::cout << "Angelscript: " << elapsed << std::endl;
    if( r != asEXECUTION_FINISHED )
    {
        if( r == asEXECUTION_EXCEPTION )
        {
            printf("An exception '%s' occurred. Please correct the code and try again.\n", ctx->GetExceptionString());
        }
    }

    ctx->Release();
    asMatrices->Release();
    engine->Release();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    uint matrixCount = 50;

    QVector<QMatrix4x4> matrices(matrixCount);

    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(-1, 1);

    for (int i=0; i<matrices.size(); ++i)
    {
        float* data = matrices[i].data();
        for (int j=0; j<16; ++j)
        {
            data[j] = distribution(generator);
        }
    }

    QMatrix4x4 result;
    for (int i=0; i<matrices.size(); ++i)
    {
        result *= matrices.at(i);
    }

    const float* resultData = result.constData();
    for (int i=0; i<16; ++i)
    {
        std::cout << resultData[i] << ' ';
    }
    std::cout << std::endl;

    uint testNumber = 100;
    testAngelScript(matrices, testNumber);
    testQtScript(matrices, testNumber);

    return 0;
}
