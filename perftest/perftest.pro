#-------------------------------------------------
#
# Project created by QtCreator 2014-09-20T12:35:25
#
#-------------------------------------------------

QT       += gui core script

QMAKE_CXXFLAGS += -O2 -std=c++11

TARGET = perftest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -langelscript

SOURCES += main.cpp \
    scriptarray/scriptarray.cpp \
    scriptbuilder/scriptbuilder.cpp

HEADERS += \
    scriptarray/scriptarray.h \
    scriptbuilder/scriptbuilder.h
